# 欢迎

## 关于

使用mkdocs(theme material)管理内容，cloudflare pages实现持续部署，感谢开发者们的付出。

UPD：目前作者处于半退游状态，数值基础值得一看，其它内容大概率跟不上版本更迭。尽管如此，关于数值基础有不同见解欢迎到仓库中提issue和PR！会尽快回复。

本篇主要记录本人在游戏《封印者》上遇到过的困惑，也希望作为中文区的一份数值指南和模型分析，作为攻略的补充。如果有误，麻烦大家提PR指正，谢谢大家。  
当前，中文区攻略百花齐放，但大多零散且以私有格式（Q群，图片，贴吧，B站专栏/视频）呈现。该笔记同时以markdown源代码和网站的形式公布，希望能给社区做一点微小的贡献。  
同时，还在编写的本篇并不完善，如果需要系统的数据百科，更建议使用下面的友链closersinfo。  
台服，韩服，日服群体相对相比大陆服更少对线更多分析，加入closersinfo discord是一个好的起点。  

点击左侧菜单上的栏目以阅读本文档。  
请注意文档更新日期以保证时效性。  

给新人的建议：看本文档前过一遍下面任一wiki的“回归/新手指南”。  

我向天发誓，正文中绝对不会出现这该死的翻译腔。

## 友链

### wiki

- [Closers info（日语）](https://closersinfo.jp/) 非常系统，基础数值全
- [BWiki](https://wiki.biligame.com/closers/%E9%A6%96%E9%A1%B5)  本地化

- [closersinfo.xyz（繁中）](https://closersinfo.xyz/) 台人做的wiki，相比jp有服装，爆抗数据和更准确的数值理解
- [namu.wiki(韩文)](https://namu.wiki) 看上去很厉害 但还不会用


### 专题

- [220805 角色特点强度大致整理](https://tieba.baidu.com/p/7960148838)
- [220911 白PVE攻略](https://tieba.baidu.com/p/8016335815)
    - [PVE相关心得](https://docs.qq.com/doc/DWkpRbFhmZ0JPQU5Q)
    - [主动技能说明](https://docs.qq.com/sheet/DWldzUUhKd0hERXZV?tab=BB08J2)

### 攻略者们

- [千坂雪樱：萌新入坑基础教程/Boss流程讲解](https://space.bilibili.com/7418277)
- [迷途の塔塔：数值科普/硬件科普/低配战术家](https://space.bilibili.com/385077345)
- [镜游御影：数值理解/方向教学](https://space.bilibili.com/1499829)
- [傲娇娇official：数值测试/关卡流程分析](https://space.bilibili.com/7782025)
